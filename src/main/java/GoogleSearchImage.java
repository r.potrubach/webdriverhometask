import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.List;

public class GoogleSearchImage {
    WebDriver driver;
    @FindBy(xpath = "//input[@class='gLFyf gsfi']")
    private WebElement searchBar;
    @FindBy(xpath = "//div[@class ='FPdoLc lJ9FBc']//input[@class='gNO89b']")
    private WebElement searchButton;
    @FindBy(xpath = "//div[@class='hdtb-mitem']//a[contains(text(), 'Картинки')]")
    private WebElement imagesOfSearchingQueryButton;
    @FindBy(xpath = "//div[@class='bRMDJf islir']")
    private List<WebElement> listOfImages;
    @FindBy(xpath = "//div[@class='bRMDJf islir']//img")
    private List<WebElement> listOfImagesWithTagImg;

    public GoogleSearchImage (WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public void searchByKeyWord(String keyWord){
        searchBar.sendKeys(keyWord);
        searchButton.click();
    }

    public void imagesOfSearchingQueryButtonClick(){
        imagesOfSearchingQueryButton.click();
    }

    public int getCountOfListOfImagesWithTagImg() {
        return listOfImagesWithTagImg.size();
    }

    public int getCountOfListOfImages(){
        return listOfImages.size();
    }


}
