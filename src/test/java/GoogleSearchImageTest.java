import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertEquals;
import static org.testng.AssertJUnit.assertTrue;

public class GoogleSearchImageTest extends BaseTest{
    private static final String query = "sunset";

    @Test
    public void check (){
        getGoogleSearchImage().searchByKeyWord(query);
        getGoogleSearchImage().imagesOfSearchingQueryButtonClick();
        assertEquals(getGoogleSearchImage().getCountOfListOfImages(), getGoogleSearchImage().getCountOfListOfImagesWithTagImg());
    }

}
